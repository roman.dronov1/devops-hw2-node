import {DateRangeInput, DateSingleInput, Datepicker} from '@datepicker-react/styled';
import React , {useReducer} from 'react';
let count = 0;
const initialState = {
  startDate: null,
  endDate: null,
  focusedInput: null,
}
let dateInput={
  startDate:null,
  endDate:null,
  focusedInput:null,
  count:0,
}
 
function reducer(state, action) {
  switch (action.type) {
    case 'focusChange':
      return {...state, focusedInput: action.payload}
    case 'dateChange':
      return action.payload
    default:
      throw new Error()
  }
}
 function handleDateInput(state,getdate){
   console.log(dateInput);
   console.log(state);
   console.log(dateInput.startDate!=state.startDate);
console.log(dateInput.endDate!=state.endDate);
console.log((dateInput.startDate!=state.startDate)||(dateInput.endDate!=state.endDate));
   if((dateInput.startDate!=state.startDate)||(dateInput.endDate!=state.endDate)||(dateInput.focusedInput!=state.focusedInput)){
     //dateInput.count=0;
  dateInput.focusedInput=state.focusedInput;
   dateInput.startDate=state.startDate;
   dateInput.endDate=state.endDate;
   console.log("count "+count);
   console.log(state);
   console.log((state.startDate!=null));
   console.log(state.endDate!=null);
   console.log(state.focusedInput===null);
   console.log((state.startDate!=null ||state.endDate!=null)&&state.focusedInput===null);
   if((state.startDate!=null ||state.endDate!=null)&&state.focusedInput===null){
     console.log("getDate sent");
    // if(count<1) {
       count++;
       getdate(state);
    // }
   }
  }
 }
function Calendar({getdate}) {
  const [state, dispatch] = useReducer(reducer, initialState)
  handleDateInput(state,getdate);
 
  return (
    <DateRangeInput
      onDatesChange={data => dispatch({type: 'dateChange', payload: data})}
      onFocusChange={focusedInput => dispatch({type: 'focusChange', payload: focusedInput})}
      startDate={state.startDate} // Date or null
      endDate={state.endDate} // Date or null
      focusedInput={state.focusedInput} // START_DATE, END_DATE or null
    />
  )
}
export default Calendar;