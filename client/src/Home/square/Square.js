import React from 'react';
import './Square.css';
import Flip from 'react-reveal/Flip';
import Fade from 'react-reveal/Fade';
export default class Square extends React.Component{

    render(){
        let host="localhost";
        let imgUrl=this.props.img;
        let directionLeft=this.props.directionLeft;
        let directionRight=this.props.directionRight;
        let city=this.props.city;
        let price=this.props.price;
        let squareStyle={
            
            backgroundColor: `rgba(51, 51, 51,${this.props.opacity})`,
        };
        let style={
            backgroundImage:`url(${process.env.PUBLIC_URL+imgUrl})`,
        }
        return(
            <div className="Square" style={squareStyle}>
                <Fade left={directionLeft} right={directionRight} duration="500"><div className="photo" style={style}>
                    <Fade delay={600}>
                    <div className="text">{city}</div>
                    <div className="price">from {price}</div>
                </Fade></div></Fade>
                </div>
        );
    }
}