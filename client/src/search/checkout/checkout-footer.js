import React from 'react';
import './checkout-footer.css';
import Fade from 'react-reveal/Fade';
import OrderSummary from './order-summary/order-summary';
export default class CheckoutFooter extends React.Component{
        constructor(props){
            super(props);
            this.state={
                style:{
                height:'100px',
            },
                openCheckout:false,
            };
        }
summaryClose(){
    this.setState({style:{height:'100px', transition:'height 0.3s ease-out',},openCheckout:false,});
}
openCheckout(){
    console.log('clicked');
this.setState({style:{height:'90%', transition:'height 0.3s ease-out',},openCheckout:true,});
}
    render(){
        let flightPrice=this.props.flight.price.substring()||null;
        let flightPriceNum=flightPrice.substring(0,flightPrice.length-1)||null;
        let footerInfo=[];
        if(this.state.openCheckout){
footerInfo.push(<OrderSummary hotelName={'Deribasovskaya village'}
originCity={'Moscow'}
destinationCity={'Odessa'}
departureDate={'23.01.14'}
returnDate={'23.01.14'}
sum={+flightPriceNum+(+this.props.hotel.price)+'$'}
summaryClose={()=>this.summaryClose()}/>);
        }
        else{
        let footer=<div></div>;
        let total;
        if(this.props.flight!=null){
           footer=<div className="flight-price">{this.props.flight.price}</div>
        }
        if(this.props.hotel!=null){
            footer=<div className="flight-price">{this.props.hotel.price}</div>
        }
        if((this.props.flight!=null)&&(this.props.hotel!=null)){
            let flightPrice=this.props.flight.price.substring();
            let flightPriceNum=flightPrice.substring(0,flightPrice.length-1);
        footer=<div className="flight-price">{this.props.flight.price} + {this.props.hotel.price}$</div>;
total=<div className="total-price">Total: {+flightPriceNum+(+this.props.hotel.price)}$</div>;

        }
        footerInfo.push(footer);
            footerInfo.push(total);
    }
        return(<Fade bottom><div className="checkout-footer" style={this.state.style}>
          {footerInfo}
          <button onClick={()=>this.openCheckout()}>Proceed</button>
        </div></Fade>);
    }
}