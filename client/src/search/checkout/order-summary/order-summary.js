import React from 'react';
export default class OrderSummary extends React.Component{
    constructor(props){
super(props);
this.hotelName=this.props.hotelName|| '';
this.originCity=this.props.originCity|| '';
this.destinationCity=this.props.destinationCity|| '';
this.departureDate=this.props.departureDate|| '';
this.returnDate=this.props.returnDate||null;
this.sum=this.props.sum|| '';
this.wingyCardNum=this.getWingyCardNumber();
this.wingyCardBalance=this.getWingyCardBalance();
    }
    getWingyCardBalance(){
        return '430$';
    }
    getWingyCardNumber(){
        return '4533';
    }
    render(){
        return(
            <div>
{this.sum}
{this.hotelName}
{this.originCity}
{this.destinationCity}
{this.wingyCardNum}
<button onClick={this.props.summaryClose}>close</button>
            </div>
        )
    }
}
