import React from 'react';
import './App.css';
import Card from "./Card";
import ReccommendationBlock from "./ReccommendationBlock";
import Error from "./Error";
import Fade from 'react-reveal/Fade';
import CheckoutFooter from "./checkout/checkout-footer"
const host="localhost";
class Search extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        data:false,
        error:false,
        hotelClick:null,
        flightClick:null,
        footerCheckout:null,
    }
    this.json={};
  }
  componentDidMount() {
   this.requestData();
  }
  handleCardFlightClick(price,time){
console.log("Card click");
console.log(price,time);
this.setState({
  footerCheckout:true,
  flightClick:{
  price:price,
}});
  }
  handleCardHotelClick(price){
    this.setState({
      footerCheckout:true,
      hotelClick:{
      price:price,
    }});
  }
  async requestData(){
      let urlStr=window.location.href;
      let url=new URL(urlStr);
      let origin=url.searchParams.get("origin");
      let destination=url.searchParams.get("destination");
      let departureDate=url.searchParams.get("departureDate");
      let returnDate=url.searchParams.get("returnDate");
      let urlGET=`http://${host}:3001/scripts?origin=${origin}&destination=${destination}&departureDate=${departureDate}`;
      if(returnDate){
          urlGET+="&returnDate="+returnDate;
      }

      try {
       // const json = await this.getRequestData('/scripts?origin='+origin+'&destination='+destination+'&departureDate='+departureDate);
          const json = await this.getRequestData(urlGET);
        //const json=await this.getRequestData('output.json');
         console.log("json");
          console.log(json);// JSON-string from `response.json()` call
          this.json=json;
          if("errors" in this.json){
              this.setState({error:true});
              console.log("errorSet");
          }
          else {
              console.log("dataSet");
              this.setState({data: true});
          }
      } catch (error) {
          console.error(error);
      }
  }
  async getRequestData(url){
      const response = await fetch(url);
      if(response.ok) {
          console.log("response ok");
          return response.json();
      }
      else{
          alert("Mistake HTTP:"+response.status);
      }
  }
  render()
  {
    let page=[];
    let pageHotel=[];
    let reccommendBlock="";
    console.log(this.state.data);
    if(this.state.data===true) {
      let json=this.json;
      let tickets = json.tickets;
      let ticketsAmount=tickets.ticketsAmount;
      reccommendBlock= <ReccommendationBlock json={json}/>;
      const cards = [];
      for (let i = 0; i < ticketsAmount; i++) {
        cards.push(<Card key={i} ticket={tickets.tickets[i]} id={i} onclick={(a,b)=>this.handleCardFlightClick(a,b)}/>);
      }
      page=cards.slice();
      let hotel=json.hotel.array;
      for(let i=0;i<hotel.length;i++){
          pageHotel.push(<Card key={i} hotel={hotel[i]} id={i} onclick={(a)=>this.handleCardHotelClick(a)}/>)
      }
    }
    else if(this.state.error===true){
        let json=this.json;
        page.push(json.errors.status);
        page.push(json.errors.title);
    }
    else{
      page='some info';
    }
    let checkoutFooter;
    if(this.state.footerCheckout){
      checkoutFooter=<CheckoutFooter flight={this.state.flightClick} hotel={this.state.hotelClick}/>;

    }
    return (
        <div>
          <div>{reccommendBlock}</div>
        <div className="Flight">{page}</div>
            <div className="Hotel">{pageHotel}</div>
            {checkoutFooter};
        </div>
    );
  }
}

//export default App;
export default Search;
