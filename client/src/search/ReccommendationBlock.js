import React from 'react';
import ReccommendationField from "./ReccommendationField";
import "./ReccommendationBlock.css";
 class ReccommendationBlock extends React.Component{
     constructor(props){
         super(props);
     }
     render() {
         let json=this.props.json;
         let tickets=json.tickets;
         let hotel=json.hotel;
         let hotelArray=hotel.array;
         console.log("RecBlock");
         console.log(json);
         console.log("OptimalIndexHotel"+hotel.optimalIndexID);
         console.log("hotelArray");
         console.log(hotelArray[0]);
         return(
             <div className={"reccommendationBlock"}>
             <ReccommendationField ticket={tickets.tickets[0]} hotel={hotelArray[0]} isMinimal={true}/>
             <ReccommendationField ticket={tickets.tickets[tickets.optimalIndexId-1]} hotel={hotel.optimalIndex} isOptimal={true}/>

             </div>
         )
     }
 }
 export default ReccommendationBlock;