import React from 'react';
import './Card.css';

class Card extends React.Component{
parseSegments(segments){
    let str=[];
   for(let i=0;i<segments.length;i++){
       if(i===0||segments[i-1].arrival.airport!=segments[i].departure.airport) str.push(segments[i].departure.airport);
       str.push(" "+segments[i].arrival.airport+" ");

   }
   return str;
}

render() {
    //console.log("ticket");
    //console.log(this.props.ticket);
    //console.log("hotel");
    //console.log(this.props.hotel);
    let isOptimal=this.props.isOptimal;
    let isMinimal=this.props.isMinimal;
   // console.log(this.props.id);
    console.log(isOptimal);

    let styles={
        backgroundColor:(isOptimal||isMinimal)?"red":"white",
    }
    let card=[];
    if(this.props.ticket){
        console.log(this.props.ticket);
for(let i=0;i<this.props.ticket.flight.length;i++){
    let segments=this.props.ticket.flight[i].segments||null;
    let departure=this.props.ticket.flight[i].segments[0].departure.airport||null;
    let arrival=this.props.ticket.flight[i].segments[0].arrival.airport||null;
    let price=(this.props.ticket.totalPrice+"$")||null;
    let point=this.props.ticket.point||null;
    let time=this.props.ticket.flight[i].totalduration||null;
    let segmentsBlock=this.parseSegments(segments)||null;
    let id=this.props.id||null;
    card.push(<div className={'Card'} style={styles} onClick={()=>this.props.onclick(price,time)}>{segmentsBlock} <br/>
        {time}
        <br/>
        {this.props.id}
        <br/>
{id}
        <br/>
        {price}
        <br/>
        {point}
    </div>);
}

    }
    if(this.props.hotel){
        let hotel=this.props.hotel;
      card.push( <div className={'Card'} style={styles} onClick={()=>this.props.onclick(hotel.price)}>
            {hotel.name}
            <br/>
            {hotel.stars+" stars"}
            <br/>
            {hotel.price+" $"}
            <br/>
            {hotel.center+" km"}
            <br/>
            {hotel.point}
        </div>);
    }
console.log("Card");
    //console.log(card);
    card.push(<br/>);
    return(
    /*  <div className={'Card'} style={styles}>
            {segmentsBlock}
            <br/>
            {time}
            <br/>
            {this.props.id}
            <br/>
            {price}
            <br/>
            {point}
        </div>*/
    card
    )
}
}
export default Card;