var http = require('http');
var express=require('express');
var app = express();
var url = require('url');
//var test=require('./test.html');
//--------------for fake db json resutl
const fs = require('fs');
let testJson = fs.readFileSync('test.json');

//-----------------------------

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
  res.send('hello world')
});
app.get('/scripts/', function (req, res) {
  console.log("url");
  let origin=req.query.origin;
  let destination=req.query.destination;
  let departureDate=req.query.departureDate;
  if((origin===null)||(destination===null)||(departureDate===null)){
    let errors={};
    errors.status="404";
    errors.title="INVALID_VARIABLE";
    res.send({errors});
  }
  else{
  console.log(req.query.departureDate);
  var request = require("request");
  console.log(req.query.origin);
  console.log(req.query.destination);
auth(function(accesstoken){
  console.log(accesstoken);
  console.log('test.api.amadeus.com/v2/shopping/flight-offers?originLocationCode='+origin+'&destinationLocationCode='+destination+'&departureDate='+departureDate+'&adults=1&nonStop=false');
  let getdates={ method: 'GET',
  url: 'https://api.amadeus.com/v2/shopping/flight-offers?originLocationCode='+origin+'&destinationLocationCode='+destination+'&departureDate='+departureDate+'&adults=1&nonStop=false',
  headers: 
   { 'cache-control': 'no-cache',
     'Content-Type': 'application/x-www-form-urlencoded' ,
     'Authorization': 'Bearer '+accesstoken
    }, };
     request(getdates, function (error, response, body) {
      if (error) {throw new Error(error);
      console.log(error);}
      else{
        console.log(body);
        let jsonBody=JSON.parse(body);
        if("errors" in jsonBody){
          let errors={};
          errors.status=jsonBody.errors[0].status;
          errors.title=jsonBody.errors[0].title;
          res.send({errors});
        } 
        else{
      let flights=setPoints(JSON.parse(body));
      let fligthsRespond=formJSONforCards(flights);
res.send(JSON.stringify(fligthsRespond));
        }
}});
});
//let flights=setPoints(JSON.parse(testJson));
//let fligthsRespond=formJSONforCards(flights);
//res.send(JSON.stringify(fligthsRespond));
//res.send(fligthsRespond);

  }});

  //res.send('u gay');
app.get('/scripts', function (req, res) {
  res.send('u gay x2');
});
app.listen(3001);
function auth(callback){
  var request = require("request");
var authObj;
  var options = { method: 'POST',
    url: 'https://api.amadeus.com/v1/security/oauth2/token',
    headers: 
     { 'cache-control': 'no-cache',
       'Content-Type': 'application/x-www-form-urlencoded' },
    form: 
     { grant_type: 'client_credentials',
       client_id: 'L5V11jUMrWjJGAt9C2hfX6PsgdG8pAAt',
       client_secret: 'GuCGPcdI8IGiCFnN' } };
  
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    authObj=JSON.parse(body);
    console.log(authObj.access_token);
    callback(authObj.access_token);
});
}
function setPoints(array){
  let lowestPoint={point:200000,
    id:0};
  for(let i=0;i<array.data.length;i++){
      array.data[i].point=definePoints(array.data[i]);
      if(array.data[i].point<lowestPoint.point){
          lowestPoint.point=array.data[i].point;
          lowestPoint.id=array.data[i].id;
          array.optimalIndexId=lowestPoint.id;
      }
      console.log("LowestPoint"+lowestPoint.id+" "+lowestPoint.point);
  }
  return array;
}
function definePoints(array){
  let durationStr=array.itineraries[0].duration;
  console.log(durationStr);
  const durationPoint=parseFloat(durationParse(durationStr));
  let segments=parseFloat(array.itineraries[0].segments.length);
  let price=parseFloat(array.price.total);
  let pricePoint=price/10;
  // let segmentPoint=segmentpointCalc(segments);
  let segmentPoint=segments*10;
  console.log("price "+price+" segments "+segments+" duration "+durationPoint);
  //console.log(typeof(price)+typeof(segments)+typeof(durationPoint));
  let mult=pricePoint+segmentPoint+durationPoint;
  console.log(mult);
  return mult;
}
function durationParse(durationStr){
  var timeArray= durationStr.split("H");
  var hours=timeArray[0].split('PT');
  var minutes=timeArray[1].split('M');
  if(minutes[0].length===1){
      let minStr=".0"+minutes[0];
      return hours[1]+minStr;
  }
  if(minutes[0]===""){
      return hours[1];
  }
  if(minutes[0].length===2){
      return hours[1]+"."+minutes[0];
  }
}
function segmentpointCalc(segmnets){
  if(segmnets===1){
      return 0;
  }
  else{
      let value=segmnets*10;
      return value;
  }
}
function formJSONforCards(array) {
  let json = {};
  let tickets = [];

  let segments=[];
  console.log("Optimal Index"+array.optimalIndexId);
  json.optimalIndexId=array.optimalIndexId;
  json.ticketsAmount = array.meta.count;
  for (let i = 0; i < array.data.length; i++) {

      if (array.data[i].numberOfBookableSeats) tickets.push({seatsLeft: array.data[i].numberOfBookableSeats});
      tickets[i].point=array.data[i].point;
      if (array.data[i].price.total) tickets[i].totalPrice = array.data[i].price.total;
      if (array.data[i].price.currency) tickets[i].currency = array.data[i].price.currency;
      if (array.data[i].price.additionalServices) {
          let additionalServices = [];
          for (let y = 0; y < array.data[i].price.additionalServices.length; y++) {
              if (array.data[i].price.additionalServices[y].amount) additionalServices.push({amount: array.data[i].price.additionalServices[y].amount});
              if (array.data[i].price.additionalServices[y].type) additionalServices[y].type = array.data[i].price.additionalServices[y].type;
          }
          tickets[i].additionalServices = additionalServices.slice();
      }

      if(array.data[i].pricingOptions.includedCheckedBagsOnly) tickets[i].checkedBags=array.data[i].pricingOptions.includedCheckedBagsOnly;
      let flight = [];
      for(let j=0;j<array.data[i].itineraries.length;j++) {

          flight.push({totalduration: array.data[i].itineraries[j].duration});
          let segments=[];
          for (let x = 0; x < array.data[i].itineraries[j].segments.length; x++) {
              let departure={};
              let arrival={};
              departure.airport=array.data[i].itineraries[j].segments[x].departure.iataCode;
              departure.city=getCity(array.data[i].itineraries[j].segments[x].departure.iataCode);
              departure.terminal=array.data[i].itineraries[j].segments[x].departure.terminal;
              departure.date=getDateJson(array.data[i].itineraries[j].segments[x].departure.at);
              departure.time=getTimeJson(array.data[i].itineraries[j].segments[x].departure.at);
              arrival.airport=array.data[i].itineraries[j].segments[x].arrival.iataCode;
              arrival.city=getCity(array.data[i].itineraries[j].segments[x].arrival.iataCode);
              arrival.terminal=array.data[i].itineraries[j].segments[x].arrival.terminal;
              arrival.date=getDateJson(array.data[i].itineraries[j].segments[x].arrival.at);
              arrival.time=getTimeJson(array.data[i].itineraries[j].segments[x].arrival.at);
              segments.push({departure,arrival});

            /*  let checkedBags=[];
              for(let y=0;y<array.data[i].travelerPricings.length;y++){
                  json.tickets[i].flight[j].segments[x].checkedBags=[];
                  json.tickets[i].flight[j].segments[x].checkedBags.push({amount:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.quantity,
                      weight:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.weight,
                      unit:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.weightUnit,});
                  json.tickets[i].flight[j].segments[x].cabin=[];
                  json.tickets[i].flight[j].segments[x].cabin.push(array.data[i].travelerPricings[y].fareDetailsBySegment[x].cabin);

              }
              segments*/
              segments[x].airline=getAirline(array.data[i].itineraries[j].segments[x].carrierCode);
              segments[x].flightNum=array.data[i].itineraries[j].segments[x].carrierCode+array.data[i].itineraries[j].segments[x].number;
              segments[x].aircraft=getAircraft(array.data[i].itineraries[j].segments[x].aircraft.code);
              console.log("i"+i);
           //   console.log(array.data[i].id);
           //   console.log(array.data[i].itineraries[j].segments[x].id);

              if("operating" in array.data[i].itineraries[j].segments[x]) {
                  if (array.data[i].itineraries[j].segments[x].carrierCode != array.data[i].itineraries[j].segments[x].operating.carrierCode) {
                      segments[x].operatingAirline = getAirline(array.data[i].itineraries[j].segments[x].operating.carrierCode);
                  }
              }
              segments[x].segmentDuration=array.data[i].itineraries[j].segments[x].duration;
          }
          flight[j].segments=segments.slice();

      }
      tickets[i].flight=flight.slice();
     //tickets[i].segments=semgents.slice();

  }
 json.tickets = tickets.slice();
// console.log(json);
 //console.log(json.tickets[1].flight);
 let jsonObj=JSON.stringify(json);
  var fs = require('fs');
  fs.writeFile('output.json', jsonObj, 'utf8',(err) => {
      if (err) throw err;
      console.log('The file has been saved!');
  });
  return json;
}
function getCity(iata){
  return iata;
}
function getDateJson(dateStr){
  let datearray=dateStr.split("T");
  return datearray[0];
}
function getTimeJson(dateStr){
  let datearray=dateStr.split("T");
  return datearray[1];
}
function getAirline(code){
 // let obj={iata:"LAX",}
// let name =airports.findWhere(function (obj) {
//     return obj.get('name');
     
// });
 // console.log(name);
  return code;
}
function getAircraft(code){
  return code;
}