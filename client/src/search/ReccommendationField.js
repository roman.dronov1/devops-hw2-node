import React from 'react';
import Card from "./Card";
import "./ReccommendationField.css";
class ReccommendationField extends React.Component{
    render() {
        let ticket=this.props.ticket;
        let hotel=this.props.hotel;
        let isMinimal=this.props.isMinimal;
        let isOptimal=this.props.isOptimal;
        let name;
        console.log("Hotel Filed rec");
        console.log(hotel);
        console.log(ticket);
        if(isMinimal){
            name="Cheapest";
        }
        else if(isOptimal){
            name="Optimal";
        }
        return(
            <div className={"reccommendationField"}>
                <div className={"name"}>{name}</div>
            <Card key={455} ticket={ticket} isMinimal={isMinimal} isOptimal={isOptimal}/>
            <Card key={745} hotel={hotel} isMinimal={isMinimal} isOptimal={isOptimal}/>
            </div>
        )
    }

}
export default ReccommendationField;