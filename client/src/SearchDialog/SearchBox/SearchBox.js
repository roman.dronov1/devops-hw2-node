import React,{useReducer} from 'react';
import './SearchBox.css';
import InputField from './InputField/InputField';
import Calendar from './DatePicker/DatePicker';
export default class SearchBox extends React.Component{
  
      render() {
       
        /*let searchButton=<Link to={this.getSearchLink()}><button>Search</button></Link>;
        if(this.state.checkURLprarms.getResult===true){
          searchButton= <Link to={this.getSearchLink()}><button>Search</button></Link>;
        }*/
        return(<div className="SearchBox">
          <InputField id={"1"} getCode={(i)=>this.props.setOrigin(i)}/>
          <InputField id={"2"} getCode={(i)=>this.props.setDestination(i)}/>
          <Calendar getdate={(a)=>this.props.getDate(a)}/>
         </div>);
      
      }
}