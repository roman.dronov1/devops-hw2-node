import React from 'react';
import "./SearchDialog.css";
import SearchBox from './SearchBox/SearchBox';
import {Link} from 'react-router-dom';
import Fade from 'react-reveal/Fade';
export default class SearchDialog extends React.Component{
    constructor(props){
        super(props);
        this.departureDate=null;
        this.returnDate=undefined;
        this.departure=null;
        this.destination=null;
        this.state={
        searchText:'Where to next?',
          departureDate:null,
          returnDate:null,
          destination:null,
          departure:null,
          searchDropdown:null,
          checkURLprarms:{
            origin:false,
            destination:false,
            departureDate:false,
            arrivalDate:false,
            getResult(){
              return this.origin || this.destination||this.departureDate||this.arrivalDate;
            }
    
          }
        }
    
      }
      /** Objects has property toString() that returns date in a format for Amadeus and booking.com systems */
    setTripDates(dateArray){
      console.log(dateArray);
     this.departureDate={
        day:dateArray[0].getDate(),
        month:dateArray[0].getMonth()+1,
        year:dateArray[0].getFullYear(),
        toString() {
          if(this.month<10&&this.day<10){
            return this.year+"-0"+this.month+"-0"+this.day;
          }
          else if(this.month<10&&this.day>10){
            return this.year+"-0"+this.month+"-"+this.day;
          }
          else if(this.month>10&&this.day<10){
            return this.year+"-"+this.month+"-0"+this.day;
          }
          else {
            return this.year + "-" + this.month + "-" + this.day;
          }
        }};
      if(dateArray.length===2) {
      this.returnDate = {
        day: dateArray[1].getDate(),
        month: dateArray[1].getMonth()+1,
        year: dateArray[1].getFullYear(),
        toString() {
          if(this.month<10&&this.day<10){
            return this.year+"-0"+this.month+"-0"+this.day;
          }
          else if(this.month<10&&this.day>10){
            return this.year+"-0"+this.month+"-"+this.day;
          }
          else if(this.month>10&&this.day<10){
            return this.year+"-"+this.month+"-0"+this.day;
          }
          else {
            return this.year + "-" + this.month + "-" + this.day;
          }
        }
      };
      let tmp=this.returnDate.toString();
      this.returnDate=tmp;
      //this.setState({returnDate:true});
    }
    let tmp=this.departureDate.toString();
    this.departureDate=tmp;
    //this.setState({departureDate:true});
    console.log("getDate");
    //console.log(this.departureDate);
    //console.log(this.returnDate);
    
    }
      setOrigin(iata){
        this.setState({departure:iata});
        this.departure=iata;
        this.setState({checkURLprarms:{origin:true}});
        console.log("origin"+iata);
      }
      getDate(date){
        console.log("GETDATETEST STATE");
        console.log(date);
        let dates=[];
        if(date.startDate){
          console.log("startDate");
          let parsedStartDate=new Date(date.startDate);
          this.setState({checkURLprarms:{departureDate:true}});
          //console.log(parsedStartDate);
          //console.log(`${parsedStartDate.getFullYear()}-${parsedStartDate.getMonth()}-${parsedStartDate.getDate()}`);
          dates.push(parsedStartDate);
        }
        if(date.endDate){
          let parsedEndDate=new Date(date.endDate);
          console.log("endDate");
         // console.log(parsedEndDate);
        //  console.log(`${parsedEndDate.getFullYear()}-${parsedEndDate.getMonth()}-${parsedEndDate.getDate()}`);
       this.setState({checkURLprarms:{arrivalDate:true}});
          dates.push(parsedEndDate);
          console.log(dates);
        }
        this.setTripDates(dates);
    
      }
      setDestination(iata){
        this.setState({destination:iata});
        this.destination=iata;
        this.setState({checkURLprarms:{destination:true}});
        console.log("destination"+iata);
      }
      getSearchLink(){
        let departureDate=this.departureDate.toString();
        let urlStr = `/search/?origin=${this.departure}&destination=${this.destination}&departureDate=${departureDate}`;
      if (this.returnDate) {
        urlStr += `&returnDate=${this.returnDate.toString()}`;
      }
      console.log("link"+urlStr);
      return urlStr;
        //return `/search/?origin=${this.departure}&destination=${this.destination}&departureDate=2020-01-20`;
      }
      handleSearchBoxClick(){
this.setState({searchDropdown:true});
      }
    render(){
        console.log("render");
        let searchDropdown=null;
       if(this.state.searchDropdown){
searchDropdown=<Fade duration={500} top><div className="dropdown"><SearchBox setOrigin={(i)=>this.setOrigin(i)} setDestination={(i)=>this.setDestination(i)} getDate={(a)=>this.getDate(a)}/></div></Fade>;
       }
        let searchButton= <Link to={`/search/?origin=${this.departure}&destination=${this.destination}&departureDate=${this.departureDate}`}><div className="button">Search <img src={`${process.env.PUBLIC_URL}/icons/SearchDialog/arrow.svg`}/></div></Link>;
        if ((this.state.checkURLprarms.getResult===true)||this.departureDate||this.returnDate) {
         console.log("if emited");
         let link=`/search/?origin=${this.departure}&destination=${this.destination}&departureDate=${this.departureDate}`;
          if(this.returnDate){
            link+=`&returnDate=${this.returnDate}`;
    
          }
          searchButton=<Link to={link}><div className="button">Search <img src={`${process.env.PUBLIC_URL}/icons/SearchDialog/arrow.svg`}/></div></Link>;
        }
        console.log(searchButton);
        console.log(this.departureDate);
        console.log(this.returnDate);
        return(
            <div className="SearchDialog">
                <div className="Search" onClick={()=>this.handleSearchBoxClick()} >{this.state.searchText}</div>
                {searchDropdown}
                {searchButton}
            </div>
        )
    }
}