import React from 'react';
import Search from './search/Search';
import Home from './Home';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';

class App extends React.Component{
    render(){
         return (
             <div>
                 <Router>
                     <Switch>
                     <Route path="/" exact component={Home}/>
                     <Route path="/search" component={Search}/> 
                     </Switch>
                 </Router>
             </div>
         );
    }
}
export default App;