var http = require('http');
var express = require('express');
var app = express();
var url = require('url');
const fetch = require("node-fetch");
//var test=require('./test.html');
//--------------for fake db json resutl
const fs = require('fs');
const cors = require('cors');
//let testJson = fs.readFileSync('test.json');
let jsonAiports = fs.readFileSync('airports.json');

app.use(cors());

//-----------------------------

// respond with "hello world" when a GET request is made to the homepage
app.get('/',function(req,res){
  console.log("hello");
res.send("hello");
});
app.get('/scripts/', function (req, res) {
  console.log("url");
  console.log(req);
  let origin = req.query.origin;
  let destination = req.query.destination;
  let departureDate = req.query.departureDate;
  let returnDate = req.query.returnDate;
  let daysStay = 1;
  let urlstr = 'https://api.amadeus.com/v2/shopping/flight-offers?originLocationCode=' + origin + '&destinationLocationCode=' + destination + '&departureDate=' + departureDate;
  if (returnDate) {
    urlstr += '&returnDate=' + returnDate;
    daysStay = calculateDays(departureDate, returnDate);
  }
  urlstr += '&adults=1&nonStop=false';
  console.log(urlstr);
  if ((origin === null) || (destination === null) || (departureDate === null)) {
    let errors = {};
    errors.status = "404";
    errors.title = "INVALID_VARIABLE";
    res.send({ errors });
  }
  else {
    console.log(req.query.departureDate);
    var request = require("request");
    console.log(req.query.origin);
    console.log(req.query.destination);
    auth(function (accesstoken) {
      console.log(accesstoken);
      console.log('test.api.amadeus.com/v2/shopping/flight-offers?originLocationCode=' + origin + '&destinationLocationCode=' + destination + '&departureDate=' + departureDate + '&adults=1&nonStop=false');
      let getdates = {
        method: 'GET',
        //url: 'https://api.amadeus.com/v2/shopping/flight-offers?originLocationCode='+origin+'&destinationLocationCode='+destination+'&departureDate='+departureDate+'&adults=1&nonStop=false',
        url: urlstr,
        headers:
        {
          'cache-control': 'no-cache',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ' + accesstoken
        },
      };
      request(getdates, function (error, response, body) {
        if (error) {
          throw new Error(error);
          console.log(error);
        }
        else {
          //console.log(body);
          let jsonBody = JSON.parse(body);
          if ("errors" in jsonBody) {
            let errors = {};
            errors.status = jsonBody.errors[0].status;
            errors.title = jsonBody.errors[0].title;
            res.send({ errors });
          }
          else {
            let flights = setPoints(JSON.parse(body));
            let fligthsRespond = formJSONforCards(flights);
            let hotels = createHotels(daysStay);
            let tickets = fligthsRespond;
            let json = {};
            json.tickets = tickets;
            json.hotel = hotels;
            //save
            let jsonObj = JSON.stringify(json);
            let fs = require('fs');
            console.log(jsonObj);
            fs.writeFile('output_hotel.json', jsonObj, 'utf8', (err) => {
              if (err) throw err;
              console.log('The file has been saved!');
            });
            //savened
            res.send(JSON.stringify(json));
          }
        }
      });
    });
    //let flights=setPoints(JSON.parse(testJson));
    //let fligthsRespond=formJSONforCards(flights);
    //res.send(JSON.stringify(fligthsRespond));
    //res.send(fligthsRespond);

  }
});
app.get('/airports', function (req, res) {
  let letters = req.query.str;
  let json = JSON.parse(jsonAiports);
  console.log("json");
  console.log(json.length);
  console.log("-----------------");
  let searchResults = [];
  for (let i in json) {
    let lnt = letters.length;
    let objTmp = {};
    let jsonCityStr = json[i].city.slice(0, lnt);
    let jsonIATAStr;
    let jsonAirportStr = json[i].name.slice(0, lnt);
    if (json[i].iata != null) {
      jsonIATAStr = json[i].iata.slice(0, lnt);
    }
    if (letters === jsonCityStr || letters === jsonIATAStr || letters === jsonAirportStr) {
      if (json[i].iata != "") {
        objTmp.name = json[i].name;
        objTmp.city = json[i].city;
        objTmp.iata = json[i].iata;
        searchResults.push(objTmp);
      }
    }

  }
  res.send(JSON.stringify(searchResults));
});
app.listen(3001);
function auth(callback) {
  var request = require("request");
  var authObj;
  var options = {
    method: 'POST',
    url: 'https://api.amadeus.com/v1/security/oauth2/token',
    headers:
    {
      'cache-control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    form:
    {
      grant_type: 'client_credentials',
      client_id: 'L5V11jUMrWjJGAt9C2hfX6PsgdG8pAAt',
      client_secret: 'GuCGPcdI8IGiCFnN'
    }
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    authObj = JSON.parse(body);
    console.log(authObj.access_token);
    callback(authObj.access_token);
  });
}
function setPoints(array) {
  let lowestPoint = {
    point: 200000,
    id: 0
  };
  for (let i = 0; i < array.data.length; i++) {
    array.data[i].point = definePoints(array.data[i]);
    if (array.data[i].point < lowestPoint.point) {
      lowestPoint.point = array.data[i].point;
      lowestPoint.id = array.data[i].id;
      array.optimalIndexId = lowestPoint.id;
    }
    console.log("LowestPoint" + lowestPoint.id + " " + lowestPoint.point);
  }
  return array;
}
function definePoints(array) {
  let durationStr = array.itineraries[0].duration;
  console.log(durationStr);
  const durationPoint = parseFloat(durationParse(durationStr));
  let segments = parseFloat(array.itineraries[0].segments.length);
  let price = parseFloat(array.price.total);
  let pricePoint = price / 10;
  // let segmentPoint=segmentpointCalc(segments);
  let segmentPoint = segments * 10;
  console.log("price " + price + " segments " + segments + " duration " + durationPoint);
  //console.log(typeof(price)+typeof(segments)+typeof(durationPoint));
  let mult = pricePoint + segmentPoint + durationPoint;
  console.log(mult);
  return mult;
}
function durationParse(durationStr) {
  var timeArray = durationStr.split("H");
  var hours = timeArray[0].split('PT');
  var minutes = timeArray[1].split('M');
  if (minutes[0].length === 1) {
    let minStr = ".0" + minutes[0];
    return hours[1] + minStr;
  }
  if (minutes[0] === "") {
    return hours[1];
  }
  if (minutes[0].length === 2) {
    return hours[1] + "." + minutes[0];
  }
}
function segmentpointCalc(segmnets) {
  if (segmnets === 1) {
    return 0;
  }
  else {
    let value = segmnets * 10;
    return value;
  }
}
function formJSONforCards(array) {
  let json = {};
  let tickets = [];

  let segments = [];
  console.log("Optimal Index" + array.optimalIndexId);
  json.optimalIndexId = array.optimalIndexId;
  console.log(array.meta.count);
  json.ticketsAmount = array.meta.count;
  for (let i = 0; i < array.data.length; i++) {

    if (array.data[i].numberOfBookableSeats) tickets.push({ seatsLeft: array.data[i].numberOfBookableSeats,id:array.data[i].id });
    tickets[i].point = array.data[i].point;
    if (array.data[i].price.total) tickets[i].totalPrice = array.data[i].price.total;
    if (array.data[i].price.currency) tickets[i].currency = array.data[i].price.currency;
    if (array.data[i].price.additionalServices) {
      let additionalServices = [];
      for (let y = 0; y < array.data[i].price.additionalServices.length; y++) {
        if (array.data[i].price.additionalServices[y].amount) additionalServices.push({ amount: array.data[i].price.additionalServices[y].amount });
        if (array.data[i].price.additionalServices[y].type) additionalServices[y].type = array.data[i].price.additionalServices[y].type;
      }
      tickets[i].additionalServices = additionalServices.slice();
    }

    if (array.data[i].pricingOptions.includedCheckedBagsOnly) tickets[i].checkedBags = array.data[i].pricingOptions.includedCheckedBagsOnly;
    let flight = [];
    for (let j = 0; j < array.data[i].itineraries.length; j++) {

      flight.push({ totalduration: array.data[i].itineraries[j].duration });
      let segments = [];
      for (let x = 0; x < array.data[i].itineraries[j].segments.length; x++) {
        let departure = {};
        let arrival = {};
        departure.airport = array.data[i].itineraries[j].segments[x].departure.iataCode;
        departure.city = getCity(array.data[i].itineraries[j].segments[x].departure.iataCode);
        departure.terminal = array.data[i].itineraries[j].segments[x].departure.terminal;
        departure.date = getDateJson(array.data[i].itineraries[j].segments[x].departure.at);
        departure.time = getTimeJson(array.data[i].itineraries[j].segments[x].departure.at);
        arrival.airport = array.data[i].itineraries[j].segments[x].arrival.iataCode;
        arrival.city = getCity(array.data[i].itineraries[j].segments[x].arrival.iataCode);
        arrival.terminal = array.data[i].itineraries[j].segments[x].arrival.terminal;
        arrival.date = getDateJson(array.data[i].itineraries[j].segments[x].arrival.at);
        arrival.time = getTimeJson(array.data[i].itineraries[j].segments[x].arrival.at);
        segments.push({ departure, arrival });

        /*  let checkedBags=[];
          for(let y=0;y<array.data[i].travelerPricings.length;y++){
              json.tickets[i].flight[j].segments[x].checkedBags=[];
              json.tickets[i].flight[j].segments[x].checkedBags.push({amount:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.quantity,
                  weight:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.weight,
                  unit:array.data[i].travelerPricings[y].fareDetailsBySegment[x].includedCheckedBags.weightUnit,});
              json.tickets[i].flight[j].segments[x].cabin=[];
              json.tickets[i].flight[j].segments[x].cabin.push(array.data[i].travelerPricings[y].fareDetailsBySegment[x].cabin);

          }
          segments*/
        segments[x].airline = getAirline(array.data[i].itineraries[j].segments[x].carrierCode);
        segments[x].flightNum = array.data[i].itineraries[j].segments[x].carrierCode + array.data[i].itineraries[j].segments[x].number;
        segments[x].aircraft = getAircraft(array.data[i].itineraries[j].segments[x].aircraft.code);
        console.log("i" + i);
        //   console.log(array.data[i].id);
        //   console.log(array.data[i].itineraries[j].segments[x].id);

        if ("operating" in array.data[i].itineraries[j].segments[x]) {
          if (array.data[i].itineraries[j].segments[x].carrierCode != array.data[i].itineraries[j].segments[x].operating.carrierCode) {
            segments[x].operatingAirline = getAirline(array.data[i].itineraries[j].segments[x].operating.carrierCode);
          }
        }
        segments[x].segmentDuration = array.data[i].itineraries[j].segments[x].duration;
      }
      flight[j].segments = segments.slice();

    }
    tickets[i].flight = flight.slice();
    //tickets[i].segments=semgents.slice();

  }
  json.tickets = tickets.slice();
  // console.log(json);
  //console.log(json.tickets[1].flight);
  let jsonObj = JSON.stringify(json);
  var fs = require('fs');
  fs.writeFile('output.json', jsonObj, 'utf8', (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
  return json;
}
function getCity(iata) {
  return iata;
}
function getDateJson(dateStr) {
  let datearray = dateStr.split("T");
  return datearray[0];
}
function getTimeJson(dateStr) {
  let datearray = dateStr.split("T");
  return datearray[1];
}
function getAirline(code) {
  // let obj={iata:"LAX",}
  // let name =airports.findWhere(function (obj) {
  //     return obj.get('name');

  // });
  // console.log(name);
  return code;
}
function getAircraft(code) {
  return code;
}

/*Hotel function block
 *
 *Create hotels array and set points
 *defauls sort by price (low to high)
 */
function createHotels(daysStay) {
  let min_Price = 25;
  let max_Price = 500;
  let min_center = 0;
  let max_center = 30;
  let low_point = {
    id: 0,
    point: 1000,
  };
  class Hotel {
    constructor(name, stars = 3, price = 100, center = 5, facilities = null) {
      this.name = name;
      this.stars = stars;
      this.price = price;
      this.center = center;
      this.facilities = facilities;
      this.point = 0;
      this.id = 0;
    }
    static toJSON() {

    }
  }
  let hotelRandom = {
    adj: ["Royal", "Center", "Ancient", "Radisson", "Azimut", "Fountain", "Airport", "Cozy", "Winter", "Luxurious", "Centruion", "European", "Russian", "American", "French", "Park"],
    noun: ["Palace", "Kempinskii", "Watson", "Garden", "Museum", "Season", "City", "home", "house", "Apartments", "rooms", "Indigo", "Capital", "Avenue", "Pound", "Embankment", "Club"],
    get name() {
      return new Hotel(this.adj[Math.floor(Math.random() * this.adj.length)] + " " + this.noun[Math.floor(Math.random() * this.noun.length)], Math.floor(Math.random() * 6), (Math.floor(Math.random() * (max_Price - min_Price) + min_Price)) * daysStay, Math.floor(Math.random() * (max_center - min_center) + min_center));
    }
  }
  let hotels = {};
  hotels.array = [];
  for (let i = 0; i < 100; i++) {
    hotels.array.push(hotelRandom.name);
    hotels.array[i].point = setPointsHotel(hotels.array[i]);
    hotels.array[i].id = i + 1;
    if (hotels.array[i].point < low_point.point) {
      low_point.point = hotels.array[i].point;
      low_point.id = i + 1;
      low_point.obj = hotels.array[i];
    }

  }
  hotels.array.sort((a, b) => { if (a.price > b.price) return 1; if (a.price < b.price) return -1; return 0; })
  //console.log(hotels);
  // console.log(low_point);
  hotels.optimalIndexID = low_point.id;
  hotels.optimalIndex = low_point.obj;
  return hotels;
}
function setPointsHotel(hotel) {
  let price = hotel.price;
  let center = hotel.center;
  let stars = hotel.stars;
  let point = ((price / 10) + (center)) - ((stars >= 2) ? stars : 0);
  return point;
}
function calculateDays(to, from) {
  let dateTo = new Date(to);
  let dateFrom = new Date(from);
  console.log(Math.round((dateFrom - dateTo) / (1000 * 60 * 60 * 24)));
  return Math.round((dateFrom - dateTo) / (1000 * 60 * 60 * 24));
}